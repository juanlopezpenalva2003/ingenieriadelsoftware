using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject PantallaDeCarga;
    public Slider Slider;
    
    public void CargarNivel(string nombreNivel)
    {
        StartCoroutine(CargarAsync(nombreNivel));
    }
    IEnumerator CargarAsync(string nombreNivel)
    {
        AsyncOperation Operacion = SceneManager.LoadSceneAsync(nombreNivel);
        PantallaDeCarga.SetActive(true);
        while (!Operacion.isDone)
        {
            float Progreso = Mathf.Clamp01(Operacion.progress / .9f);
            Slider.value = Progreso;
            yield return null;
        }
    }
    public void Salir()
    {
        Application.Quit();
    }
}
