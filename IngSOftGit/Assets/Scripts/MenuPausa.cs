using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPausa : MonoBehaviour
{
    public GameObject GrupoMenuPausa;
    public bool Pausa = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Pausa == false)
            {
                GrupoMenuPausa.SetActive(true);
                Pausa = true;
                Time.timeScale = 0;
            }
        }
    }
    public void Resumir()
    {
        GrupoMenuPausa.SetActive(false);
        Pausa = false;
        Time.timeScale = 1;
    }
    public void CargarNivel(string nombreNivel)
    {
        SceneManager.LoadScene(nombreNivel);
    }
    public void Salir()
    {
        Application.Quit();
    }
}
